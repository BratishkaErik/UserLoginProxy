# UserLoginProxy
Autologin feature for [UserLogin](https://www.spigotmc.org/resources/userlogin.80669/) 2.12.1+ and [NukeLogin](https://www.spigotmc.org/resources/nukelogin.108611/), requires **Java 11+** and [Velocity](https://velocitypowered.com/) 3.0.0+ or [BungeeCord](https://www.spigotmc.org/go/bungeecord)
